

# necessary imports
import nltk
from nltk.stem import WordNetLemmatizer
import json
import random
import os


# variables
lemmatizer = WordNetLemmatizer()
stop_words = ['?', '!']
directory_in_question = os.path.abspath(os.path.dirname(__file__))
data_file = open(os.path.join(directory_in_question, 'cat_intents.json')).read()
intents_and_responses = json.loads(data_file)


def get_user_utterance_initial():
    user_utterance = input('How can I help you today? ')
    token = nltk.word_tokenize(user_utterance)
    token = [lemmatizer.lemmatize(w.lower()) for w in token if w not in stop_words]
    return token


def get_user_utterance_subsequent():
    user_utterance = input('Anything else? ')
    token = nltk.word_tokenize(user_utterance)
    token = [lemmatizer.lemmatize(w.lower()) for w in token if w not in stop_words]
    return token


def reply_according_to_intent():
    played = 0
    while 1 < 2:
        temporary_list = []
        if played == 0:
            user_utterance = get_user_utterance_initial()
        else:
            user_utterance = get_user_utterance_subsequent()
        for block in intents_and_responses['intents']:
            for pattern in block['patterns']:
                token_2 = nltk.word_tokenize(pattern)
                token_2 = [lemmatizer.lemmatize(w.lower()) for w in token_2 if w not in stop_words]
                temporary_list.extend(token_2)
                if set(user_utterance).issubset(set(temporary_list)):
                    print (random.choice(block['responses']))
                    temporary_list = []
                    played += 1
                else:
                    temporary_list = []
        user_utterance = " ".join(user_utterance)
        user_utterance = user_utterance[0].upper() + user_utterance[1:]
        if user_utterance in intents_and_responses['intents'][1]['patterns']:
            break


reply_according_to_intent()
